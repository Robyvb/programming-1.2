const username = localStorage.getItem("username");
const navBar = document.getElementById("navBar");
const button = document.createElement("button");

if(username == null){
    //user is logged out --> present sign up button
    button.innerText = "Sign up";

    navBar.appendChild(button)
    button.addEventListener("click", function(){
        location.href = "signup.html";
    });
}else{
    //user is logged in --> display their name and present a sign out button
    const usernameText = document.createElement("span");
    const button = document.createElement("button");
    usernameText.innerText = "Welcome, " + username;
    navBar.appendChild(usernameText);

    button.innerText = "Sign out";
    navBar.appendChild(button);

    button.addEventListener("click", function(){
        localStorage.removeItem("username");
        location.reload();
    });
}
