let passwordStrength = 0;
let isValid = false;

const submitForm = document.getElementById("submitForm");
const passwordField = document.getElementById("password");
const usernameErr = document.getElementById("usernameErr");
const birthdayErr = document.getElementById("birthdayErr");
const passErr = document.getElementById("passErr");
const passConfErr = document.getElementById("passConfErr");
const agreeErr = document.getElementById("agreeErr");

submitForm.addEventListener(
  "click",
  function (event) {
    validate();
    if (!isValid) {
      event.preventDefault();
      console.log("Not submitting! Still some false fields.");
    } else {
      localStorage.setItem("username", myForm.username.value);
      return true;
    }
  },
  false
);

passwordField.addEventListener("change", checkPassword);

function checkPassword() {
  password = passwordField.value;
  passwordStrength = 0;

  if (password.match(/[a-z]+/) && password.match(/[A-Z]+/)) {
    //console.log("Contains a capital and non capital character");
    passwordStrength++;
  }

  if (password.match(/.*[0-9]+/)) {
    //console.log("Contains a digit");
    passwordStrength++;
  }

  if (password.match("!", ",", "'", ";", ".", "-", "?")) {
    //console.log("Contains punctuation");
    passwordStrength++;
  }
  if (password.match(/^.{6,}$/)) {
    //minimum check, if the password's length is less than 6 characters then change the
    //strength to 0
    //Can also be checked simply with .length
    //--> if(password.length >= 6)
    //console.log("Length is at least 6");
    passwordStrength++;
  } else {
    passwordStrength = 0;
  }

  //console.log(passwordStrength);
  passwordStrengthFiller();
}

function passwordStrengthFiller(){
  document.getElementById("button1").style.backgroundColor = "";
  document.getElementById("button2").style.backgroundColor = "";
  document.getElementById("button3").style.backgroundColor = "";
  document.getElementById("button4").style.backgroundColor = "";
  let colorBackground = "";
  
  switch(passwordStrength){
    case 1:
      colorBackground = "Red";
      break;  
    case 2:
      colorBackground = "Orange";
      break;
    case 3:
      colorBackground = "Yellow";
      break;
    case 4:
      colorBackground = "Green";
      break;
  }

  switch(passwordStrength){
    case 4:
      //console.log("Het is 4 :)");
      document.getElementById("button4").style.backgroundColor = colorBackground;
    case 3:
      //console.log("Het is 3 of groter :)");
      document.getElementById("button3").style.backgroundColor = colorBackground;
    case 2:
      //console.log("Het is 2 of groter :)");
      document.getElementById("button2").style.backgroundColor = colorBackground;
    case 1:
      //console.log("Het is 1 of groter :)");
      document.getElementById("button1").style.backgroundColor = colorBackground;
  }
}

function validate() {
  const currentYear = new Date().getFullYear();
  const originalPassword = passwordField.value;
  const confirmPassword = document.getElementById("confpassword").value;
  const usernameValue = myForm.username.value;
  const birthdayValue = myForm.birthday.value;
  const checkbox = document.getElementById("agreeCheckbox")

  isValid = true;
  ///^[a-zA-Z][0-9a-zA-Z]{6,}$/
  if (!usernameValue.match(/.{4,}$/)) {
    //alert("The length must be 4 or higher!");
    usernameErr.innerText = "The length must be 4 or higher!";
    isValid = false;
  } else if (!usernameValue.match(/^[0-9a-zA-Z]{4,}$/)) {
    //alert("Only letters and numbers are allowed!");
    usernameErr.innerText = "Only letters and numbers are allowed!";
    isValid = false;
  } else if (!usernameValue.match(/^[a-zA-Z][0-9a-zA-Z]{3,}$/)) {
    //alert("Please start your username with a letter.");
    usernameErr.innerText = "Please start your username with a letter.";
    isValid = false;
  }else{
    usernameErr.innerText = "";
  }

  //correct format: YYYY-MM-DD --> 2000-11-15
  if (!birthdayValue.match(/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/)) {
    //alert("Please provide your date of birth in this format: [YYYY-MM-DD]");
    birthdayErr.innerText = "Please provide your date of birth in this format: [YYYY-MM-DD]";
    isValid = false;
  } else if (
    //check if the month is between 1 and 12
    parseInt(birthdayValue.substring(6, 7)) > 12 ||
    parseInt(birthdayValue.substring(6, 7)) < 1
  ) {
    //alert("The month must be between 1 and 12");
    birthdayErr.innerText = "The month must be between 1 and 12";
    isValid = false;
  } else if (
    //check if the day is between 1 and 31
    parseInt(birthdayValue.substring(9, 10)) > 31 ||
    parseInt(birthdayValue.substring(9, 10)) < 1
  ) {
    //alert("The day must be between 1 and 31");
    birthdayErr.innerText = "The day must be between 1 and 31";
    isValid = false;
    //check if the year is in the past (compare the first 4 characters to the current year)
  } else if (parseInt(birthdayValue.substring(0, 4)) > currentYear) {
    //alert("Please fill a year that's in the past.");
    birthdayErr.innerText = "Please fill a year that's in the past.";
    isValid = false;
  }else{
    birthdayErr.innerText = "";
  }

  if (passwordStrength < 1) {
    //alert("The minimum length is 6.");
    passErr.innerText = "The minimum length is 6.";
    isValid = false;
  }else{
    passErr.innerText = "";
  }

  if (confirmPassword != originalPassword) {
    //alert("The passwords must match.");
    passConfErr.innerText = "The passwords must match.";
    isValid = false;
  }else{
    passConfErr.innerText = "";
  }

  if (checkbox.checked != true) {
    //alert("Please check that you agree with everything.");
    agreeErr.innerText = "Please check that you agree with everything.";
    isValid = false;
  }else{
    agreeErr.innerText = "";
  }
}
